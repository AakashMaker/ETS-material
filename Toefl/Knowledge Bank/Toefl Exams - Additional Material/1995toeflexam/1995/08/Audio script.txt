1995��8�� �и���������

1. Do you want to go on a trip with us to Florida this spring?
It will cost about $300 a person.
What can be inferred about the man?

2. My watch stopped again. And I just got a new battery. 
Why don't you take it to Smith's Jewelry. They can check it for you. And they're pretty reasonable.
What does the man mean?  

3. We're going to change our meeting from Monday to Tuesday.
It's all the same to me. 
What does the man mean?  

4. We plan to go to the beach after class. Want to come?
I'd love to. But Prof. Jones want to speak with me.
What will the woman probably do?

5. Janet sounded worried about her grades.
But she's getting As & Bs, isn't she?
What does the man imply about Janet?

6. You look great since you've been taking those exercises classes.
Thanks. I've never felt better in my life.
What does the man imply?

7. I had a hard time getting through this novel.
I know how you feel. Who can remember the names of 35 different characters.
What does the woman imply?

8. That's a long line. Do you think there'll be any tickets left?
I doubt it. Guess we'll wind up going to the second show.
What does the woman mean?

9. This course in much too hard for me. 
Sorry you decided to take it, huh?
What does the man ask the woman?

10. Are you going home for winter vacation?
I'd agreed to stay on here as a research assistant.
What can be inferred about the woman?

11. Hello!
Hello! This is Dr, Grey's office. We're calling to remind you of your 4:15 appointment for your annual checkup tomorrow.
Oh, thanks. It's a good thing that you called. I thought it was 4:15 today.
What does the man mean?

12. How wonderful you won the scholarship. Can you believe it?
No. It's almost too good to be true.
What does the man mean?  

13. Excuse me. Prof. Davidson. But I was hoping to talk to you about my class project for economics.
I have a class in a few minutes. Why don't you come to see me during office hours tomorrow?
When will the woman discuss her project with Prof. Davidson?

14. How are you feeling?
The stuff the nurse gave me seemed to have helped. But it's making me awfully drowsy.
What does the woman mean?  

15. Bill Smith has volunteered to write a summary of the proposals we've agreed on.
Will I have a chance to review it?
What does the woman want to know?

16. Why don't you wear that yellow shirt that your sister gave you for your birthday.
I love that shirt. But it's missing two buttons.
What does the man mean?

17. How many classes do you have today?
Just one. From 3 till 6.
What does the man mean?

18. Our football team didn't play very well.
That's true. But at least we won the game.
What does the man mean?

19. This has been an unusually cool summer.
Uh huh! I actually had to get out my wool sweaters in August. 
What does the woman imply?

20. I got some bad news today. The store where I work in laying off staff.
Are they going to let you go?
What does the woman want to know?

21. I'd like to pick this film up by 4 tomorrow afternoon.
I can have it for you at 2 if you like.
What does the woman say about the film?

22. I talked to Philip today and he said he'd be coming to the party.
Oh, so he can come after all.
What can be inferred about Philip?

23. Gary insists on buying the food for the picnic.
That's pretty generous. But shouldn't we at least offer to share the expense?
What does the woman suggest they do?

24. How's the new job going?
Well. I'm getting used to lots of new things. But I wish the supervisor would give me some feedback.
What does the woman mean?

25. Did Linda ever finish that introductory chapter?
I'm not sure. She's spent hours on end rewriting it.
What does the man imply about Linda?

26. The supermarket down the street is selling everything half price because they are going out of business.
Sounds like an ideal time to stock up on coffee.
What does the man mean?

27. Have you heard anything about the new professor?
Just that she's no pushover.
What does the man say about the professor?

28. I need to get a copy of my birth certificate.
Sorry. But we can only accept requests by mail now.
What does the woman mean?

29. When is the earliest flight from Washington to New York?
There's a shuttle at six. And if that's full, there's another at 7.
What does the man mean?  

30. How do you like to help me plan the refreshments for the astronomy club meeting tomorrow night?
Sure. Let's be careful not to overdo it though. Last time we had enough for 3 clubs put together.
What does the woman mean?


Question 31-33
David, can I give you a hand with one of those grocery bags?
Sure, Nanny. Could you take this one please? I didn't realize how heavy these bags would be.
Why did you buy so much stuff when you have to walk back home from the store?
Well, I didn't intend to buy a lot. But I'm having some people over and I guess I needed more than I expected.
What's the occasion?
Now the people I live with, the Kremers, have been on vacation for a month and I thought I'd surprise them. I'm inviting some of their friends and families for a welcome home dinner.
Oh, that's really thoughtful of you.
I figure it's the least I can do for them. They've been letting me stay with them rent free while I'm in school. 
Really? That's pretty generous of them.
Well, they understand how difficult it is to make ends meet when you're a student. They've been such a big help to me. I thought that this might be a small way to thank them for the generosity.
31. What is David trying to do?
32. Why did David think he wouldn't have a problem?
33. Why is David appreciative of the Kremers?

Question 34-37
Wonderful I spent most of my time at the art museum. I especially liked the new wing. I was amazed to hear the guide explain the problems they had building it.
Right. I just read an article that went on & on about the cost. 90 million total I think.
Yeah. The guide mentioned that. You could see they spared no expense.
Hm. It looked really unusual, at least from what I saw in the picture.
It is. The basic design is two triangles. In fact there are triangles all over. The paving stones in the courtyard, the skylights and even a lot of the sculptures.
One sculpture is a mobile. It's in the courtyard and it's made of pieces of aluminum that moves slowly in the air. It's really impressive.
That was in the article too. It said that the original was steel and it weighed so much that it wasn't safe to hand.
Right. They did it over in aluminum so it wouldn't come crashing down on someone's head.
You know the article went into that in detail. There was even an interview with the sculptor.
I'd like to read that. Would you mind if I borrow the magazine sometime?
No. I wouldn't mind if I haven't thrown it out yet.

34. What did the woman think of the new wing of the museum?
35. How had the man learned about the museum?
36. According to the woman, what do the paving stones, skylights and mobile have in common?
37. What was the problem with the original mobile?

Questions 38 to 41
In the few minutes that remain of today's class. I'd like to discuss next week's schedule with you because I'm presenting a paper at a conference in Detroit on Thursday, I won't be here for either Wednesday's or Friday's class. I will however be here for Monday's. Next Friday, a week from today, is the midterm exam, marking the half way point in the semester. Prof. Andrews has agreed to administer the exam. In place of the usual Wednesday's class, I've arranged an optional review session. Since it is optional, attendance will not be taken. However attending the class would be a good idea for those worried about the midterm. So remember: Optional class next Wednesday; Midterm, Friday.

38. What is the purpose of the talk?
39. At what point during the semester does the talk take place?
40. What did Prof. Andrews agree to do?
41. What will occur at next Wednesday's class time?

Question 42 to 46 
Today's lecture we'll center on the prehistoric people of Nevada Desert. Now most of these prehistoric desert people moved across the countryside throughout the year. You might think that they're wandering aimlessly. Far from it, they actually followed the series of carefully planned moves.
Where they moved depended on where food was available. Places where plants were ripening or fish were spawning. Now often when these people moved, they carried all their possessions on their backs. But if the journey was long, extra food and tools were sometimes stored in caves or beneath rocks. 
One of these caves is now an exciting archaeological site. Beyond its small opening is a huge underground grotto. Even though the cave is very large, it was certainly two dark and dusty for the crawlers to live in. But it was a great place to hide things. And tremendous amounts of food supplies and artifacts have been found there. The food includes dried fish seeds and nuts. The artifacts include stone spear points and knives. The spar points are actually rather small. Here is a picture of some that were found. You can see their size in relation to the hands holding them.

42. What is the main subject of this talk?
43. What point does the speaker make about the prehistoric people of the Nevada Desert?
44. Why didn't the people live in the cave described by the speaker?
45. What have archaeologists found in the cave?
46. Why does the speaker show a photo to the class?

Question 47 to 50
To us, the environment in which fish dwell often seems cold, dark and mysterious. But there are advantages to living in water. And they've played an important role in making fish what they are. One is that water isn't subject to sudden temperature changes. Therefore it makes an excellent habitat for a cold blooded animal.
Another advantage is the water's ability to easily support body weight. Protoplasm has approximately the same density as water. So a fish in water is almost weightless. This weightlessness in turn means two things:
1) A fish can get along with a light weight and a simple bone structure. And 
2) Limitations to a fish's size are practically removed. Yet there is one basic difficulty to living in water the fact that it is incompressible. For a fish to move through water, it must actually shove it aside. Most can do this by wiggling back and forth in snakelike motion. The fish pushes water aside by the forward motion of its head and with a curve of its body and its flexible tall. Next the water flows back along the fish's narrowing size, closing in at the tall and helping the fish propel itself forward. 
  The fact that water is incompressible has literally shaped the development of fish. A flat and angular shape can be moved through water only with difficulty. And for this reason, fish have a basic shape that is beautifully adapted to deal with this peculiarity.

47. What is the talk mainly about?
48. What does the speaker mention as a problem that water presents to fish?
49. The speaker compares a fish's movement with that of what creature?
50. What aspect of a fish does the speaker discuss in the most detail?
